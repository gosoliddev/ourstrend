<?php
function wptuts_scripts_basic()
{    
    // Register the script like this for a theme:
    wp_register_script( 'custom-script', get_template_directory_uri() . '/js/ourstrend.js' );
 
    // For either a plugin or a theme, you can then enqueue the script:
    wp_enqueue_script( 'custom-script' );
}

function list_post(){

    //or you can use the cat id here, check the codex for WP_Query
   	$custom_query = new WP_Query('cat=1'); //your category id 
	while ( have_posts() ) : the_post();
	echo '<li>';
		the_title();
	echo '</li>';
	endwhile;
	wp_reset_postdata();
}

add_action( 'wp_enqueue_scripts', 'wptuts_scripts_basic' );
// Register Custom Navigation Walker
require_once('wp_bootstrap_navwalker.php');
register_nav_menus( array(
    'primary' => __( 'Primary Menu', 'ourstrend' ),
) );

add_shortcode('list_post', 'list_post');
?>