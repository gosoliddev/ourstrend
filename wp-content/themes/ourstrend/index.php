<?php get_header(); ?>
<section >
    <div id="intro" class="container-fluid ">
      <div class="row ">
        <div id="myCarousel" class="carousel " data-ride="carousel"> 
          <!-- Indicators -->       
          <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>          
          </ol>
          <div class="carousel-inner">
            <div class="item active"> <img src="<?php bloginfo('template_url');?>/img/images3.jpg" style="width:100%" alt="First slide">
              <div class="container-fluid">
                <div class="carousel-caption">
                <span class="phrase">La oportunidad de nuestro futuro, la vemos en el éxito de nuestros clientes</span>
                </div>
              </div>
            </div>
                             
          </div>
          <div class="shadow"></div>          
          </div>          
      </div>              
    </div>      
  </section>  
  
<?php get_footer(); ?>