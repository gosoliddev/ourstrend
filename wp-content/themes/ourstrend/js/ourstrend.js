(function($){
  $(function(){  	 
	  // Initialize collapsible (uncomment the line below if you use the dropdown variation)	
	function fixedHeader()
	  {
	    var windowWidth = $(window).width();

	    if(windowWidth > 120 ){
	      $(window).on('scroll', function(){
	        if( $(window).scrollTop()>100 ){
	          $('.main-nav').addClass('header-fixed animated slideInDown');
	        } else {
	          $('.main-nav').removeClass('header-fixed animated slideInDown');
	        }
	      });
	    }else{
	      
	      $('.main-nav').addClass('fixed-menu animated slideInDown');
	        
	    }
	  }

	  fixedHeader();
	   //Portfolio item slider
      /*$('#portfolio-slider').bxSlider({
        mode: 'fade',
        autoControls: false
      });
	   new WOW().init();*/

 }); // end of document ready
})(jQuery);