<?php
/*
Template Name: About
*/
?>
<?php get_header(); ?>
<section>
<div id="intro" class="container-fluid ">
<div class="row ">
<div id="myCarousel" class="carousel " data-ride="carousel"><!-- Indicators -->
<div class="carousel-inner">
<div class="item active">

<img style="width: 100%;" src="<?php bloginfo('template_url');?>/img/images23.jpg" alt="First slide" />
<div class="container-fluid">
<div class="carousel-caption">
			 <?php if (pll_current_language()=="en"){?>
                <span class="phrase">The oportunity of our future is in the success of our clients.</span>
                <?php }else{?>
                <span class="phrase">La oportunidad de nuestro futuro, la vemos en el éxito de nuestros clientes</span>
                <?php }?>
	</div>
</div>
</div>
</div>
<div class="shadow"></div>
</div>
</div>
</div>
</section>  
<section class="text-center">
    
    <div class="parallax bg__foo">
        <h3 >Ourstrend</h3>         
        <div class="capture">
            ourstrend is a Company operating in Bolivia that is dedicated to technology. Original elaboration with vanguard images become users in clients, being the only ones applying a suitable technological innovation that ensure the return of your investment, generating real results. We achieve results, we offer return. We fuse technology and all you can imagine, defending simplicity and harmony to achieve desired impact.” The oportunity of our future is in the success of our clients”.
        </div>     
    </div>
    <div class="parallax bg__bar">
         <h3  >MISSION</h3>           
        <div class="capture">
            Our mission is to satisfy design and development needs about internet solutions, using different tools, taking care differentiation and targets of our clients, offering technological innovative solutions that fit their needs. We aspire to lead in quality and reliability through permanent technological updating.
        </div>    
    </div>
    <div class="parallax bg__baz">
       <h3>VISION</h3>       
        <div class="capture">
        We will keep building our future, being a competitive Company and offering technological solutions, of this way we will generate enduring associations with our clients.
        </div>        
    </div>
        
</section>
<!--Owl caroucel-->
<section>
<div class="container">
 <div class="row text-center">
      <div class="col-lg-12 col-md-12 col-sm-12 ">
        <div class="slogan-section animated fadeInUp clearfix ae-animation-fadeInUp">
        <h2>Conocemos herramientas de ultima generacion.</h2>        
       Estamos a la vanguardia en tegnologias de informacion y tenemos en mente siempre dar un servicio con tegnologia de punta.
        </div>
      </div>
    </div>
  <div class="row text-center"> 
      <div id="owl-services">              
        <div class="item"><i class="icon-java" style="font-size:15em"></i></div>
        <div class="item"><i class="icon-php" style="font-size:15em"></i></div>
        <div class="item"><i class="icon-csharp" style="font-size:15em"></i></div>
        <div class="item"><i class="icon-objc" style="font-size:15em"></i></div>
        <div class="item"><i class="icon-html" style="font-size:15em"></i></div>
        <div class="item"><i class="icon-css" style="font-size:15em"></i></div>
        <div class="item"><i class=" icon-mysql-alt" style="font-size:15em"></i></div>
        <div class="item"><i class="icon-oracle" style="font-size:15em"></i></div>
        <div class="item"><i class="icon-postgres" style="font-size:15em"></i></div>  
      </div>    
  </div>
</div>
</section>
<!--End Owl caroucel-->
   <?php if (have_posts()) : while (have_posts()) : the_post();?>    
            <?php
                global $more;	// hack to use Read More in Pages
                $more = 0;
                the_content(); 
            ?>            
    <?php endwhile; endif; ?> 
<?php get_footer(); ?>