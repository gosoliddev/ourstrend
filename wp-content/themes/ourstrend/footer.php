<footer > 	
 		<div class="shared">
			<div class="container-fluid">														
				<div class="row">
					<div class="col-md-12 text-center">
					<?php if (pll_current_language()=="en"){?>
                <h4 class="contact-title">Contact us.</h4>
                <?php }else{?>
                <h4 class="contact-title">Contactanos.</h4>
                <?php }?>
						
						<div class="contact-content">
						<?php if (pll_current_language()=="en"){?>
		                <p>You can find us on social networks.</p>
		                <?php }else{?>
		                <p>Puedes encontrarnos en las redes sociales.</p>
		                <?php }?>
							
						</div>
					</div>
				</div>			
				<div class="row">
						<div class="col-xs-3 col-md-3">
							<div class="social-content text-center">
							    <a href="https://www.facebook.com/pages/Ourstrend/1544492335797841">
							    <span class="service-icon"><i class="socicon socicon-facebook img-circle" style="visibility: visible;"></i></span>
							    </a>									    
							</div>
						</div><!--/ End first service -->
						<div class="col-xs-3 col-md-3">
							<div class="social-content text-center">
							    <a href="https://plus.google.com/108921427061608141560"><span class="service-icon"><i class="socicon socicon-google img-circle" style="visibility: visible;"></i></span></a>									   
							</div>
						</div><!--/ End 2nd service -->
						<div class="col-xs-3 col-md-3">
							<div class="social-content text-center">
							    <a href="https://twitter.com/ourstrend"><span class="service-icon"><i class="socicon socicon-twitter img-circle" style="visibility: visible;"></i></span></a>									   
							</div>
						</div><!--/ End 3rd service -->
						<div class="col-xs-3 col-md-3">
							<div class="social-content last text-center">
							    <a href="https://www.youtube.com/channel/UCLaBR8qSSnUp3OXS5hZfhVQ"><span class="service-icon"><i class="socicon socicon-youtube img-circle" style="visibility: visible;"></i></span></a>

							</div>
						</div><!--/ End 4th service -->
	    		</div>
	    		<div class="row text-center">
			 		<p class="primaryForeColor">Copyright @ ourstrend</p>
			 	</div>	
	    	</div>
		</div>		
		<!--<div class="footer">
	 		<div class="container-fluid">
		 		<div class="row">
		 			<div class="col-xs-6 col-md-6">
					 	<ul class="menu-footer">
					 		<li><a href="index.html">Inicio</a></li>
					 		<li><a href="enterprise.html">El empresa</a></li>
					 		<li><a href="services.html">Servicios</a></li>	 	 		
					 	</ul>
					 </div>
					<div class="col-xs-6 col-md-6">
					 	<img src="img/logo2.png" class="img-responsive pull-right">
					 </div>
			 	</div>
			 	<div class="row text-center">
			 		<p class="primaryForeColor">Copyright@ ourstrend</p>
			 	</div>		 		
		 	</div>
	 	</div>-->
 	</footer>    
 	<script src="<?php bloginfo('template_url'); ?>/bower_components/jquery/dist/jquery.js" type="text/javascript" charset="utf-8" ></script>
	<script src="<?php bloginfo('template_url'); ?>/bower_components/bootstrap/dist/js/bootstrap.js" type="text/javascript" ></script>
	<script src="<?php bloginfo('template_url'); ?>/js/plugins/owl-caroulcel/owl.carousel.min.js"></script>
	<script src="<?php bloginfo('template_url'); ?>/js/ourstrend.js" type="text/javascript" charset="utf-8" ></script>
 	
	
	 <script type="text/javascript">
       $(document).ready(function () {

           $("#owl-languajes").owlCarousel({

               navigation: true, // Show next and prev buttons
               slideSpeed: 400,
               paginationSpeed: 800,
               autoPlay:8000,
               // "singleItem:true" is a shortcut for:
                //items : 4, 
               // itemsDesktop : false,
               // itemsDesktopSmall : false,
               // itemsTablet: false,
               // itemsMobile : false

           });
		$("#owl-services").owlCarousel({
               navigation: true, // Show next and prev buttons
               slideSpeed: 400,
               paginationSpeed: 800,
               autoPlay:8000,
               // "singleItem:true" is a shortcut for:
                items : 3, 
               // itemsDesktop : false,
               // itemsDesktopSmall : false,
               // itemsTablet: false,
               // itemsMobile : false

           });
       });
   </script>	
</body>
</html>