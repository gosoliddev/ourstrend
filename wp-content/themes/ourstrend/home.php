<?php
/*
Template Name: Home
*/
?>
<?php get_header(); ?>
<section >
<div id="intro" class="container-fluid ">
<div class="row ">
  <div id="myCarousel" class="carousel " data-ride="carousel"> 
    <!-- Indicators -->       
    <ol class="carousel-indicators">
      <!--<li data-target="#myCarousel" data-slide-to="0" class="active"></li>          -->
    </ol>
    <div class="carousel-inner">
      <div class="item active"> <img src="<?php bloginfo('template_url');?>/img/images3.jpg" style="width:100%" alt="First slide">
        <div class="container-fluid">
          <div class="carousel-caption">
          <?php if (pll_current_language()=="en"){?>
          <span class="phrase">The oportunity of our future is in the success of our clients.</span>
          <?php }else{?>
          <span class="phrase">La oportunidad de nuestro futuro, la vemos en el éxito de nuestros clientes</span>
          <?php }?>
          </div>
        </div>
      </div>
                       
    </div>
    <div class="shadow"></div>          
    </div>          
</div>              
</div>      
</section>
<section>
  <div class="container">
    <div class="row">
    <div class="col-md-3">
    <div class="panel widget">
    <div class="panel-body text-center bg-center" style="background: url('<?php bloginfo('template_url'); ?>/img/origin/support.jpg');background-size: cover;">
    <div class="row row-table">
    <div class="col-xs-12 text-white">

    <img class="img-thumbnail img-circle thumb128" src="<?php bloginfo('template_url'); ?>/img/origin/mobile.jpg" alt="Image" />
    <h3>Design adapted to mobile devices</h3>
    </div>
    </div>
    </div>
    <div class="panel-body text-center">
    Offer your clients the benefit of a website adapted to mobile devices. The most part of the people access from mobile devices to the internet.
    </div>
    <div class="list-group">
    </div>
    </div>
    </div>
    <div class="col-md-3">
    <div class="panel widget">
    <div class="panel-body text-center bg-center" style="background: url('<?php bloginfo('template_url'); ?>/img/origin/6.jpg') ;background-size: cover;">
    <div class="row row-table">
    <div class="col-xs-12 text-white">

    <img class="img-thumbnail img-circle thumb128" src="<?php bloginfo('template_url'); ?>/img/origin/part.jpg" alt="Image" />
    <h3>Simple & elegant</h3>
    </div>
    </div>
    </div>
    <div class="panel-body text-center">
    We develop simple and elegant websites that show solid and modern concepts.
    </div>
    <div class="list-group">
    </div>
    </div>
    </div>
    <div class="col-md-3">
    <div class="panel widget">
    <div class="panel-body text-center bg-center" style="background: url('<?php bloginfo('template_url'); ?>/img/origin/augmented-reality.jpg') ;background-size: cover;">
    <div class="row row-table">
    <div class="col-xs-12 text-white">

    <img class="img-thumbnail img-circle thumb128" src="<?php bloginfo('template_url'); ?>/img/origin/augment-reality.jpg" alt="Image" />
    <h3>Augmente Reality</h3>
    </div>
    </div>
    </div>
    <div class="panel-body text-center">
    Keep in your pocket the possibility of seeing the world from another perspective
    </div>
    <div class="list-group"></div>
    </div>
    </div>
    <div class="col-md-3">
    <div class="panel widget">
    <div class="panel-body text-center bg-center" style="background: url('<?php bloginfo('template_url'); ?>/img/origin/mobile.jpg') ;background-size: cover;">
    <div class="row row-table">
    <div class="col-xs-12 text-white">

    <img class="img-thumbnail img-circle thumb128" src="<?php bloginfo('template_url'); ?>/img/origin/support_tecnical.jpg" alt="Image" />
    <h3>Technical support</h3>
    </div>
    </div>
    </div>
    <div class="panel-body text-center">
    Suitable administration guarantees effectiveness & productivity in order your investment returns.
    </div>
    <div class="list-group">
    </div>
    </div>
    </div>
    </div>
</div>
</section>


<!-- The Team -->
<section class="home-doctors clearfix">
  <div class="container">
    <div class="row text-center">
      <div class="col-lg-12 col-md-12 col-sm-12 ">
        <div class="slogan-section animated fadeInUp clearfix ae-animation-fadeInUp">
        <h2>Conoce a nuestros especialistas</h2>        
        Estamos al servicio de nuestros clientes, para dar las soluciones que se comvierten en exitos compartidos, y estamos comprometidos con ello.
        </div>
      </div>
    </div>
    <div class="row">

    <!-- entry1 -->
      <div class="col-sm-3 col-sm-offset-3 text-center doc-item">
        <div class="common-doctor animated fadeInUp clearfix ae-animation-fadeInUp"><figure><img class="doc-img animate attachment-gallery-post-single wp-post-image" src="<?php bloginfo('template_url'); ?>/img/origin/Vincen.jpg" alt="doctor-2" width="670" height="500" /></figure>
          <div class="text-content">
          <h5>Vincen Alvarado</h5>
          <!--
          <div class="for-border"></div>
          -->
          <h5><small>Team Lead</small></h5>
          </div>
        </div>
      </div>
    <!-- entry2 -->
      <div class="col-sm-3 col-sm-push-1 text-center doc-item">
        <div class="common-doctor animated fadeInUp clearfix ae-animation-fadeInUp">
          <figure>
            <img class="doc-img animate attachment-gallery-post-single wp-post-image" src="<?php bloginfo('template_url'); ?>/img/origin/RAISa.jpg" alt="doctor-2" width="670" height="500" />
          </figure>
          <div class="text-content">
            <h5>Raisa Ramirez</h5>
            <!--
            <div class="for-border"></div>
            -->
            <h5><small>Customer Relationship</small></h5>
          </div>
        </div>
      </div>            
    </div>
  </div>
</section>
<!-- The Team //-->

 
<!---Team -->
 

<!--End team -->  
<?php if (have_posts()) : while (have_posts()) : the_post();?>    
  <?php                
      the_content(); 
  ?>            
<?php endwhile; endif; ?>  
 
<?php get_footer(); ?>