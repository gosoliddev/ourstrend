<!DOCTYPE html>
<html>
<head>
<base url="<?php bloginfo('template_url'); ?>" target="_self">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<title></title>	
	<!-- let styles -->
	<link href="<?php bloginfo('stylesheet_url');?>" rel="stylesheet">                            	
</head>
<body data-twttr-rendered="true">
 	<header id="header" class="navbar-fixed-top main-nav" role="banner">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<!--/ Top info start -->
					<div class="top-info ">						
						<ul class="hidden-xs">
						<?php
							if ( function_exists( 'pll_the_languages' ) ) { //Polylang
							    $idiomas = pll_the_languages( array( 'show_names' => true, 'hide_if_empty' => false, 'force_home' => true, 'show_flags' => true, 'raw' => true ) );
							    foreach ( $idiomas as $idioma ) {
							        echo '<li><a href="'. $idioma['url'] . '" title="' . $idioma['name'] . '"  hreflang = "' . $idioma['slug'] . '">'. $idioma['flag'] .'</a></li>';
							    }
							} 
						?>
						</ul>
						<ul class="pull-right hidden-xs">
							<!--<li><i class="glyphicon glyphicon-earphone"></i> Call Us: (+591) 79378545</li>-->
							<li><a href="contact-us"><i class="glyphicon glyphicon-envelope"></i></a></li>
							<!-- Social links -->
							<li class="social-icon">
								<a title="Facebook" href="https://www.facebook.com/pages/Ourstrend/1544492335797841">
									<i class="socicon socicon-facebook"></i>
								</a>
								<a  title="Google+" href="https://plus.google.com/108921427061608141560">
									<i class="socicon socicon-google"></i>
								</a>
								<a  title="Twitter" href="https://twitter.com/ourstrend">
									<i class="socicon socicon-twitter"></i>
								</a>
								<a  title="Youtube" href="https://www.youtube.com/channel/UCLaBR8qSSnUp3OXS5hZfhVQ">
									<i class="socicon socicon-youtube"></i>
								</a>
							</li>
							<!-- Social links end-->
						</ul>

					</div><!--/ Top info end -->

					<!-- Logo start -->
					<div class="navbar-header">					
					    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					        <span class="sr-only">Toggle navigation</span>
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
					    </button>
					    <a class="navbar-brand" href="/ourstrend">
					    	<img class="img-responsive" src="<?php bloginfo('template_url');?>/img/logo1.png" alt="logo">
					    </a> 
					    <ul class="visible-xs" style="float:left;list-style:none;">
						<?php
							if ( function_exists( 'pll_the_languages' ) ) { //Polylang
							    $idiomas = pll_the_languages( array( 'show_names' => true, 'hide_if_empty' => false, 'force_home' => true, 'show_flags' => true, 'raw' => true ) );
							    foreach ( $idiomas as $idioma ) {
							        echo '<li style="float:left;margin:0px 4px;"><a href="'. $idioma['url'] . '" title="' . $idioma['name'] . '"  hreflang = "' . $idioma['slug'] . '">'. $idioma['flag'] .'</a></li>';
							    }
							} 
						?>
						</ul>	
					</div><!--/ Logo end -->

					<nav class="collapse navbar-collapse clearfix" role="navigation">
						<!--<ul class="nav navbar-nav navbar-right">
							<li class="active">
	                       		<a href="index.html">Inicio</a>
	                       	</li>
	                       	<li>
	                       		<a href="enterprise.html" >La empresa</a>
	                       	</li>
	                                    			                    
	                    </ul>-->
	          <?php
	            wp_nav_menu( array(
                'menu'              => 'primary',
                'theme_location'    => 'primary',
                'depth'             => 2,                        	
                'menu_class'        => 'nav navbar-nav navbar-right',
                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                'walker'            => new wp_bootstrap_navwalker())
            	);
            ?>

					</nav><!--/ Navigation end -->	
											
				</div><!--/ Col end -->
			</div><!--/ Row end -->
		</div><!--/ Container end -->
	</header>