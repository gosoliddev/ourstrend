<?php
/*
Template Name: Contact us
*/
?>
<?php get_header(); ?>
<section class="hidden-xs">
<div id="intro" class="container-fluid ">	
	<div class="row fix-header" >
		<!--<div class="hidden-xs col-xs-12" style="background:#333;margin:0px;">
			<div class="text-center" style="width:100%;background:rgba(255, 4, 4, 0.75);color:#fff;padding:12px 0px;">		
					<?php if (pll_current_language()=="en"){?>
	                <div style="font-size:25px;">The oportunity of our future is in the success of our clients.</div>
	                <?php }else{?>
	                <div style="font-size:25px;">La oportunidad de nuestro futuro, la vemos en el éxito de nuestros clientes</div>
	                <?php }?>
			</div>	
		</div>-->	
		
		<div class="col-xs-12 col-sm-6">		
				<img style="width: 100%;" src="<?php bloginfo('template_url');?>/img/images11.jpeg" alt="First slide" />
				<div class="shadow"></div>										
		</div>			

		<div class="visible-sm visible-md visible-lg col-xs-6 col-sm-6" >
		
			<?php if (have_posts()) : while (have_posts()) : the_post();?>    
			    <?php
			        global $more;	// hack to use Read More in Pages
			        $more = 0;
			        the_content(); 
			    ?>            
			<?php endwhile; endif; ?> 					
			

		</div>	

	</div>
	
</div>
</section>  
<section class="visible-xs">
    <div id="intro" class="container-fluid ">
      <div class="row ">
        <div id="myCarousel" class="carousel " data-ride="carousel"> 
          <!-- Indicators -->       
          <ol class="carousel-indicators">            
          </ol>
          <div class="carousel-inner">
            <img style="width: 100%;" src="<?php bloginfo('template_url');?>/img/images11.jpeg" alt="First slide" />
            <div class="shadow"></div>    
              <div class="container-fluid">
                <div class="carousel-caption">
               <?php if (pll_current_language()=="en"){?>
                <h3 >The oportunity of our future is in the success of our clients.</h3>
                <?php }else{?>
                <h3 >La oportunidad de nuestro futuro, la vemos en el éxito de nuestros clientes</h3>
                <?php }?>
                </div>
              </div>
            </div>
                             
          </div>
                
          </div>             
      </div>                      
</section> 
<br /><br />
<section>
<div class="container-fluid">
	<div class="row" >
		<div class="visible-xs col-xs-12 col-sm-6 col-md-6 col-sm-offset-4 col-md-offset-3">							
				<?php if (have_posts()) : while (have_posts()) : the_post();?>    
				        <?php
				            global $more;	// hack to use Read More in Pages
				            $more = 0;
				            the_content(); 
				        ?>            
				<?php endwhile; endif; ?> 			
		</div>
	</div>
</div>
</section>
<div class="clearfix">

</div>

<?php get_footer(); ?>