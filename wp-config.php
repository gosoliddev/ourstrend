<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'ourstrend');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'ftud.GNmU{/xN]jGU9`99L2&l$gWI3eX^DZ!<&K,#=Q+[I2]NjtE-5U(XhlBs`Ru');
define('SECURE_AUTH_KEY',  '>=8ttU3ifr7Z5Sd%jEubO_fC!a=s|#$mc&G}LV`U,Yv5vF[ Qu^0D:Zt+[kdfZB/');
define('LOGGED_IN_KEY',    'v>Z|2@CM}Vv=nFxs(Kf_;b?|KBa~Kpd.SLHFZ=(5._,ST<RuTUf@chdW}%UQBQdQ');
define('NONCE_KEY',        'iz-MhV<*]9@oh[+yK|ly/2Vud)}KY^4iA-apAl`rWr+VSS;B4T%>&Gy|EC n+K3J');
define('AUTH_SALT',        '%xd@#zY8OA7s0uh4DVn2Sb,%:D>t:Q.Y#|TC!zOepULrqs}v~e6[S|>mNwOH[-6j');
define('SECURE_AUTH_SALT', 'fm*DF*gdO,h>I03x2?|xj$TeAO?>GCO{ve65d._><zr]<|]Mn+Ny/(C(u,?/Iarj');
define('LOGGED_IN_SALT',   'BMKfz2|SmdhfR/ztEU)]gow+D_tb$yikB[d8&ay2QTkBE|v,Z*@zef[Ro/|7h>I!');
define('NONCE_SALT',       'MsJ*<W4{rPY A-GD*`#?A;V:3C;te=~kWH md$l7XjN%4Vxkm=CoB{G|:qnhVjGA');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
